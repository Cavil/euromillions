#### euromillions.py 
# A Euro Millions Lucky Dip Generator
![Demo Image](img/demo_img.png)

#### Installation
Uses Python 3.9. Requires click==7.1.2 and termcolor==1.1.0.

```git clone https://Cavil@bitbucket.org/Cavil/euromillions.git```
Setup a Python3.9 virtual environment, then run ```pip install -r requirements.txt```.



#### Usage
Generates a Lucky Dip, consisting of 5 main numbers [0-50] and 2 lucky stars [1-12].
```commandline
Usage: euromillions.py [OPTIONS]

  Generates a lucky dip including a Millionaire Maker code. Takes an integer
  or defaults to one.

Options:
  -n INTEGER  Number of lucky dips.  [required]
  --help      Show this message and exit.
```

```python3 euromillions.py -n 2``` will generate 2 Lucky Dips.
```commandline
2 lucky dips coming up!

Luck Dip 01: (01): 49 (02): 42 (03): 44 (04): 09 (05): 16 Lucky Star 1: 10 Lucky Star 2: 11 Millionaire Maker: DFCX28398 
Luck Dip 02: (01): 47 (02): 44 (03): 01 (04): 39 (05): 41 Lucky Star 1: 10 Lucky Star 2: 09 Millionaire Maker: KPHG42239
```

