import os
import random
from termcolor import colored
import string
import click


def millionaire_marker():
    """Generates a Millionaire Maker code comprised of 4 unique uppercase letters and 5 numbers. ie: JHQW77383"""
    alphabet = f"{string.ascii_letters}"
    list_alphabet = []
    mm_alpha = []
    for x in alphabet:
        list_alphabet.append(x.upper())

    while len(mm_alpha) < 4:
        random.shuffle(list_alphabet)
        char = list_alphabet.pop(random.randint(1, 26))
        if char not in mm_alpha:
            mm_alpha.append(char)
    mm_numbers = []
    while len(mm_numbers) < 5:
        num = random.randint(0, 9)
        mm_numbers.append(str(num))

    mm_alpha = ''.join(mm_alpha)
    mm_numbers = ''.join(mm_numbers)
    mm = mm_alpha + mm_numbers
    # print(f"Millionaire maker: {mm}")
    return mm


def choose_main_numbers():
    """Chooses 5 numbers from 01 to 50"""
    main_number_list = list(range(1, 51, 1))
    main_picks = []
    for n, v in enumerate(range(5)):
        random.shuffle(main_number_list)
        random.shuffle(main_number_list)
        random.shuffle(main_number_list)
        random.shuffle(main_number_list)
        random.shuffle(main_number_list)
        main_picks.append(main_number_list.pop(-1))
    return main_picks


def choose_lucky_stars():
    """Chooses 2 numbers from 01 to 12."""
    lucky_stars_picks = []
    lucky_stars = list(range(1, 12, 1))
    for n, v in enumerate(range(2)):
        random.shuffle(lucky_stars)
        random.shuffle(lucky_stars)
        random.shuffle(lucky_stars)
        random.shuffle(lucky_stars)
        random.shuffle(lucky_stars)
        lucky_stars_picks.append(lucky_stars.pop(-1))
    return lucky_stars_picks


@click.command()
@click.option('-n', default=1, required=True, type=int, help="Number of lucky dips.")
def lucky_dip(n):
    """Generates a lucky dip, including a Millionaire Maker code. Takes an integer or defaults to one."""
    _ = os.system("clear")
    print(f"{n} lucky dips coming up!\n")
    for x, y in enumerate(range(n)):
        main_picks = choose_main_numbers()
        lucky_stars_picks = choose_lucky_stars()
        mm = millionaire_marker()
        print(f"Luck Dip {str(x+1).zfill(2)}: "
              f"(01): {colored(str(main_picks[0]).zfill(2), 'blue')} "
              f"(02): {colored(str(main_picks[1]).zfill(2), 'blue')} "
              f"(03): {colored(str(main_picks[2]).zfill(2), 'blue')} "
              f"(04): {colored(str(main_picks[3]).zfill(2), 'blue')} "
              f"(05): {colored(str(main_picks[4]).zfill(2), 'blue')} "
              f"Lucky Star 1: {colored(str(lucky_stars_picks[0]).zfill(2), 'yellow')} "
              f"Lucky Star 2: {colored(str(lucky_stars_picks[1]).zfill(2), 'yellow')} "
              f"Millionaire Maker: {colored(mm, 'green')} ")


if __name__ == '__main__':
    lucky_dip()
